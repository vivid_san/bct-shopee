package com.BCT_Shopee.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.BCT_Shopee.model.MerchantModel;

public interface MerchantRepo extends JpaRepository<MerchantModel, Integer> {

}
