package com.BCT_Shopee.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.BCT_Shopee.model.IMSModel;

public interface IMSRepo extends JpaRepository<IMSModel, Integer>{

}
