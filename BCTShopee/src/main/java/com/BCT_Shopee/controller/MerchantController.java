package com.BCT_Shopee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.BCT_Shopee.model.MerchantModel;
import com.BCT_Shopee.service.MerchantService;

@RestController
public class MerchantController {
	
	
	@Autowired
	private MerchantService Mservice;
	
	@GetMapping("/getall")
	public List<MerchantModel> getallItems(){
		return Mservice.getAll();
	}
	

	
	
	@GetMapping("/getone/{roll_no}")
	public MerchantModel getone(@PathVariable("roll_no") int roll_no) {
		return Mservice.getone(roll_no);
	}
	
	
	@PostMapping("/newentry")
	public MerchantModel SaveDetail(@RequestBody MerchantModel Mmodel){
		if(Mmodel != null) {
			//Mservice.SaveMerchantdetail(Mmodel);
			System.out.println(Mmodel);
			return Mservice.SaveMerchantdetail(Mmodel);
			
		}else {
			return null;
		}
	}
	
	@PutMapping("edit/{roll_no}")
	public ResponseEntity<?> editData(@PathVariable("roll_no") int roll_no,  @RequestBody MerchantModel Mmodel) {
		if(Mmodel !=null) {
			 Mservice.editData(roll_no, Mmodel);
			 return ResponseEntity.ok("Changes Have been made");
		}else {
			return ResponseEntity.ok("unable to change");
		}
	}
	
	@DeleteMapping("/delete/{roll_no}")
	public ResponseEntity<?> DeleteRow(@PathVariable("roll_no") int roll_no) {
		Mservice.DeleteOne(roll_no);
		return ResponseEntity.ok("Deleted sucessfully");
	
	}
}
