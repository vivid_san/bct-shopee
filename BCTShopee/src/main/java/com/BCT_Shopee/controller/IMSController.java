package com.BCT_Shopee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.BCT_Shopee.model.IMSModel;
import com.BCT_Shopee.repo.IMSRepo;
import com.BCT_Shopee.service.IMSService;

@RestController
public class IMSController {
	//Ims service
	@Autowired
	private IMSService ims_service;

	
	@PostMapping("/newimsdata")
	public IMSModel save_details(@RequestBody IMSModel IMSdata ) {
		if(IMSdata != null) {
			return ims_service.Save_details(IMSdata);
		}
		return null;
	}
	@GetMapping("/allimsdata")
	public List<IMSModel> getallIMSdata(){
		return ims_service.getallIMS();
	}
	@GetMapping("/imsdata/{id}")
	public IMSModel getbyid(@PathVariable("id") int id) {
		return ims_service.getbyid(id);
	}
	@DeleteMapping("/imsdelete/{id}")
	public ResponseEntity<?> deletebyid(@PathVariable("id") int id) {
		ims_service.deleteone(id);
		return ResponseEntity.ok("successful");
	}
}
