package com.BCT_Shopee.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.BCT_Shopee.model.OrderModel;
import com.BCT_Shopee.service.OrderService;
@RestController
public class OrderController {
	//this change made by sanjay
	@Autowired
	private OrderService order_service;
	@PostMapping("/neworderdata")
	public OrderModel save_details(@RequestBody OrderModel Orderdata ) {
		if(Orderdata != null) {
			return order_service.Save_details(Orderdata);
		}
		return null;
	}
	@GetMapping("/allorderdata")
	public List<OrderModel> getallOrderdata(){
		return order_service.getallOrder();
	}
	@GetMapping("/orderdata/{id}")
	public OrderModel getbyid(@PathVariable("id") int id) {
		return order_service.getbyid(id);
		//order
	}
	@DeleteMapping("/orderdelete/{id}")
	public ResponseEntity<?> deletebyid(@PathVariable("id") int id) {
		order_service.deleteone(id);
		return ResponseEntity.ok("successful");
	}
}