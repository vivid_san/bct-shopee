package com.BCT_Shopee.controller;
	
import java.util.List;



	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.http.ResponseEntity;
	import org.springframework.web.bind.annotation.DeleteMapping;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RestController;

import com.BCT_Shopee.model.ProductModel;
import com.BCT_Shopee.service.ProductService;

	@RestController
	public class ProductController {
		@Autowired
		private ProductService product_service;
		
		
		@PostMapping("/newproductdata")
		public ProductModel save_details(@RequestBody ProductModel Productdata ) {
			if(Productdata != null) {
				return product_service.Save_details(Productdata);
			}
			return null;
	}
	@GetMapping("/allproductdata")
		public List<ProductModel> getallProductdata(){
			return product_service.getallProducts();
		}
		@GetMapping("/productdata/{id}")
		public ProductModel getbyid(@PathVariable("id") int id) {
			return product_service.getbyid(id);
		}
		@DeleteMapping("/productdelete/{id}")
		public ResponseEntity<?> deletebyid(@PathVariable("id") int id) {
			product_service.deleteone(id);
			return ResponseEntity.ok("successful");
		}
	}




