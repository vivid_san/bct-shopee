package com.BCT_Shopee.model;


	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	@Entity
	public class OrderModel {
		@Id 
		@GeneratedValue(strategy = GenerationType.AUTO)
		private int order_id;
		private String product_name;
		private int order_number;
		private int quantity;
		private String order_status;
		public OrderModel() {
			
		}
		public OrderModel(int order_id, String product_name, int order_number, int quantity, String order_status) {
			super();
			this.order_id = order_id;
			this.product_name = product_name;
			this.order_number = order_number;
			this.quantity = quantity;
			this.order_status = order_status;
		}
		public int getOrder_id() {
			return order_id;
		}
		public void setOrder_id(int order_id) {
			this.order_id = order_id;
		}
		public String getProduct_name() {
			return product_name;
		}
		public void setProduct_name(String product_name) {
			this.product_name = product_name;
		}
		public int getOrder_number() {
			return order_number;
		}
		public void setOrder_number(int order_number) {
			this.order_number = order_number;
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		public String getOrder_status() {
			return order_status;
		}
		public void setOrder_status(String order_status) {
			this.order_status = order_status;
		}
		
		
			}