package com.BCT_Shopee.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class MerchantModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int roll_no;
	private String name;
	private String dob;
	private String gender;
	private String standard;
	
	public MerchantModel() {}
	
	public MerchantModel(int roll_no, String name, String dob, String gender, String standard) {
		super();
		this.roll_no = roll_no;
		this.name = name;
		this.dob = dob;
		this.gender = gender;
		this.standard = standard;
	}
	public int getRoll_no() {
		return roll_no;
	}
	public void setRoll_no(int roll_no) {
		this.roll_no = roll_no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	

	
	
}
