package com.BCT_Shopee.service;

	import java.util.List;


	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;

	import com.BCT_Shopee.model.ProductModel;
import com.BCT_Shopee.repo.ProductRepo;



	@Service
	public class ProductService {
	@Autowired
	private  ProductRepo repo;
	public   ProductModel Save_details(ProductModel Productdata) {
		return repo.save(Productdata);
	}
	public List<ProductModel> getallProduct(){
		return repo.findAll();
	}
	public ProductModel getbyid(int id) {
		return repo.findById(id).get();
	}
	public void deleteone(int id) {
		repo.deleteById(id);
	}
	public List<ProductModel> getallProducts() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	}





