package com.BCT_Shopee.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.BCT_Shopee.model.OrderModel;

import com.BCT_Shopee.repo.OrderRepo;



@Service
public class OrderService {
	@Autowired
	private   OrderRepo repo;
	public OrderModel Save_details(OrderModel Orderdata) {
		return repo.save(Orderdata);
	}
	public List<OrderModel> getallOrder(){
		return repo.findAll();
	}
	public OrderModel getbyid(int id) {
		return repo.findById(id).get();
	}
	public void deleteone(int id) {
		repo.deleteById(id);
	}

}





