package com.BCT_Shopee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.BCT_Shopee.model.IMSModel;
import com.BCT_Shopee.repo.IMSRepo;



@Service
public class IMSService {
@Autowired
private IMSRepo repo;
public IMSModel Save_details(IMSModel IMSdata) {
	return repo.save(IMSdata);
}
public List<IMSModel> getallIMS(){
	return repo.findAll();
}
public IMSModel getbyid(int id) {
	return repo.findById(id).get();
}
public void deleteone(int id) {
	repo.deleteById(id);
}

}
