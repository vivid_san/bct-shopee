package com.BCT_Shopee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.BCT_Shopee.model.MerchantModel;
import com.BCT_Shopee.repo.MerchantRepo;

@Service
public class MerchantService {
	
	@Autowired
	private MerchantRepo Mchantrepo;
	
	public MerchantModel SaveMerchantdetail(MerchantModel Mmodel) {
		return Mchantrepo.save(Mmodel);
	}
	
	public List<MerchantModel> getAll(){
		return Mchantrepo.findAll();
	}
	
	public MerchantModel getone(int roll_no) {
		return Mchantrepo.findById(roll_no).get();
	}
	
	public void DeleteOne(int roll_no) {
		Mchantrepo.deleteById(roll_no);
	}
	

	
	public void editData(int roll_no, MerchantModel Mmodel) {
		Optional<MerchantModel> MData = Mchantrepo.findById(roll_no);
		if(MData.isPresent()) {
			MerchantModel Mmodel_to_update = MData.get();
			Mmodel_to_update.setName(Mmodel.getName());
			Mmodel_to_update.setDob(Mmodel.getDob());
			Mmodel_to_update.setGender(Mmodel.getGender());
			Mmodel_to_update.setStandard(Mmodel.getStandard());
			Mchantrepo.save(Mmodel_to_update);
		}else {
			ResponseEntity.ok("unable to update");
		}
	}
}
