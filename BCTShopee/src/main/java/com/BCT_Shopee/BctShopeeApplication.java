package com.BCT_Shopee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BctShopeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BctShopeeApplication.class, args);
	}

}
//THIS IS TEST
